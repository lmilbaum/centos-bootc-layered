# centos-bootc-layered

Images layered on top of the core [centos-bootc][5] base images.

## dev environment

Use the devcontainer to run pre-commit locally. It is already populated
with all the required tools.
Spin the devcontainer and run the command:

```bash
pre-commit run --all-files
```

## Badges

| Badge                   | Description          | Service      |
| ----------------------- | -------------------- | ------------ |
| [![Renovate][1]][2]     | Dependencies         | Renovate     |
| [![Pre-commit][3]][4]   | Static quality gates | pre-commit   |
| [![devcontainer][6]][7] | DEV Env              | devcontainer |

[1]: https://img.shields.io/badge/renovate-enabled-brightgreen?logo=renovate
[2]: https://renovatebot.com
[3]: https://img.shields.io/badge/pre--commit-enabled-brightgreen?logo=pre-commit
[4]: https://pre-commit.com/
[5]: https://github.com/CentOS/centos-bootc
[6]: https://img.shields.io/static/v1?label=devcontainer&message=enabled&logo=visualstudiocode&color=007ACC&logoColor=007ACC
[7]: https://code.visualstudio.com/docs/devcontainers/containers
